pub mod packetfrag {  
    const MAX_SEQUENCE: u8 = 0x3F;
    const FLAG_START: u8 = 0x40;
    const FLAG_END: u8 = 0x80;
    const HEADER_SIZE: usize = 1;

    use std::io as io;

    pub enum AppendResult {
        // still more packets to receive
        SequenceOpen,
        // no more packets to receive for this hunk
        SequenceDone,
        // incorrect sequence number received
        SequenceError,
        // header error, etc
        DataError,
    }

    // data to send, fragment size, fragment write callback
    pub fn fragment<W: std::io::Write>(data: &[u8], frag_size: usize, sink: &mut W) -> io::Result<usize> 
        {
            if data.len() / frag_size > (MAX_SEQUENCE as usize) {
                // data cannot be sequenced properly
                return io::Result::Err(io::Error::new(io::ErrorKind::Other, "chunk sequence out of bounds"));
            }
            for i in (0..data.len()).step_by(frag_size) {
                let mut fragment: Vec<u8> = Vec::new();
                // create fragment as
                // [header: u8], [payload: u8 ...]
                let mut header: u8 = 0x00;
                if i == 0 {
                    // set start bit if first chunk
                    header |= FLAG_START;
                }
                if i + frag_size >= data.len() {
                    // set stop bit if last chunk
                    header |= FLAG_END;
                }
                // assign sequence number 
                header |= ((i / frag_size) as u8) & MAX_SEQUENCE;
                fragment.push(header);
                // determine copy size
                let copy_sz = {
                    if data.len() - i > frag_size {
                        frag_size
                    } else {
                        data.len() - i
                    }
                };
                // fill fragment apayload
                fragment.extend_from_slice(&data[i..i + copy_sz]);
                // try to write or return error
                &sink.write(&fragment[..])?;
            }
            Ok(data.len())
    }

    // incoming fragment, output data buffer
    pub fn fragment_append(fragment: Vec<u8>, data: &mut Vec<u8>) -> AppendResult {
        if fragment.len() < HEADER_SIZE {
            AppendResult::DataError
        } else {
            let header = fragment[0];
            let seq_num = header & MAX_SEQUENCE; 
            let is_start = (header & FLAG_START) > 0;
            let is_end = (header & FLAG_END) > 0;
            if is_start && seq_num > 0 {
                // starting flag cannot be at zero sequence
                AppendResult::SequenceError
            } else {
                if fragment.len() > HEADER_SIZE {
                    // if there is data after header, append it
                    data.extend_from_slice(&fragment[HEADER_SIZE..fragment.len()]);
                }
                if is_end {
                    // if end flag is set, sequence is done
                    AppendResult::SequenceDone
                } else {
                    // if end flag is not set, there are packets to process
                    AppendResult::SequenceOpen
                }
            }
        }   
    }
}

#[cfg(test)]
mod tests {
    use super::packetfrag::fragment;
    use super::packetfrag::fragment_append;
    use super::packetfrag::AppendResult;
    use std::io;

    const FRAG_SIZE: usize = 3;

    struct Dev {
        data: Vec<u8>,
        read_data: Vec<Vec<u8>>,
    }

    impl io::Write for Dev {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            self.data.extend_from_slice(buf);
            Ok(buf.len())
        }
        fn flush(&mut self) -> io::Result<()> {
            Ok(())
        }
    }

    fn write_compare(src: Vec<u8>, control: Vec<u8>) -> bool {
        let mut dev = Dev{ data: Vec::new(), read_data: Vec::new() };
        if let io::Result::Ok(_) = fragment(&src, FRAG_SIZE, &mut dev) {
            // compare two vectors
            // if all elements are equal, return true
            if control.len() ==
                dev.data.iter().zip(&control).filter(|&(d, c)| *d == *c).count() {
                true
            } else {
                println!("Result {:?} \nControl {:?}", control, dev.data);
                false
            }
        } else {
            false
        } 
    }

    #[test]
    fn test_write_len() {
        let test_fn = || {
            let src_data: Vec<u8> = vec![1,2,3,4,5,6,7,8,9];
            let mut dev = Dev{ data: Vec::new(), read_data: Vec::new() };
            if let io::Result::Ok(c) = fragment(&src_data, FRAG_SIZE, &mut dev) {
                src_data.len() == c
            } else {
                false
            }  
        };
        assert_eq!(test_fn(), true, "written count is not equal to source");
    }

    #[test]
    fn test_generate_1_by_3_fragments() {
        let src: Vec<u8> = vec![1,2,3];
        let control: Vec<u8> = vec![ 
            0xC0, 1,2,3, 
        ];
        assert_eq!(write_compare(src, control), true, "frame generation error");
    }

    #[test]
    fn test_generate_2_by_3_fragments() {
        let src: Vec<u8> = vec![1,2,3,4,5,6];
        let control: Vec<u8> = vec![ 
            0x40, 1,2,3, 
            0x81, 4,5,6, 
        ];
        assert_eq!(write_compare(src, control), true, "frame generation error");
    }

    #[test]
    fn test_generate_3_by_3_fragments() {
        let src: Vec<u8> = vec![1,2,3,4,5,6,7,8,9];
        let control: Vec<u8> = vec![ 
            0x40, 1,2,3, 
            0x01, 4,5,6, 
            0x82, 7,8,9
        ];
        assert_eq!(write_compare(src, control), true, "frame generation error");
    }

    /*
    #[test]
    fn test_append() {
        let src_data: Vec<Vec<u8>> = vec![
            vec![0x40, 1,2,3],
            vec![0x01, 4,5,6],
            vec![0x82, 7,8,9]
        ];
        let control_data: Vec<u8> = vec![1,2,3,4,5,6,7,8,9];
        let mut dst_data: Vec<u8> = Vec::new();

        let test_fn = || -> bool {
            for fragment in src_data {
                match fragment_append(fragment, &mut dst_data) {
                    AppendResult::SequenceDone => {
                        break;
                    },
                    AppendResult::SequenceError | AppendResult::SequenceError => {
                        return false;
                    },
                    _ => {}
                }
            }
            dst_data.iter().zip(&control_data).filter(|&(dst, control)| *dst == *control).count() == control_data.len()
        };
        
        assert_eq!(test_fn(), true, 
        "appended data mismatch \nControl: {:?}\nResult: {:?}", control_data, dst_data);
    }
    */
}