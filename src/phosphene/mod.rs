extern crate hidapi;
mod packetfrag;

fn u32_to_u8_le (num: u32) -> [u8; 4] {
    [   
        ((num & 0x000000FF) >> 0)  as u8, 
        ((num & 0x0000FFFF) >> 8)  as u8, 
        ((num & 0x00FFFFFF) >> 16) as u8, 
        ((num & 0xFFFFFFFF) >> 24) as u8
    ]
}

pub mod device {
    use super::hidapi::HidApi;
    use super::packetfrag::packetfrag as pkfrag;
    use super::u32_to_u8_le;

    pub struct PhospheneDev {
        device: hidapi::HidDevice,
    }

    

    impl PhospheneDev {
        pub fn create(api: HidApi) -> Option<PhospheneDev> {
            const VID: u16 = 0x0483;
            const PID: u16 = 0x5750;
            match api.open(VID, PID) {
                Ok(dev) => {
                    Some(PhospheneDev { device: dev })
                },
                Err(_) => {
                    None
                }
            }
        }
    
        // read timeout
        const fn read_timeout_ms() -> i32 {
            10
        }
    
        // report size in usb device incl report id
        const fn rep_size() -> usize {
            64
        }
    
        // valuable data size in report
        const fn rep_payload_size() -> usize {
            PhospheneDev::rep_size() - 1
        }
    
        // write report using device handle
        fn write(&self, data: Vec<u8>) -> Result<usize, &'static str> {
            const REP_SIZE: usize = PhospheneDev::rep_size();
            let mut arr: [u8; REP_SIZE] = [0; REP_SIZE];
            // first byte is report id
            arr[0] = 0x01;
            // copy data to report
            arr[1..REP_SIZE].copy_from_slice(&data[0..data.len()]);
            // write report or get error
            match self.device.write(&arr) {
                Err(e) => {
                    println!("{}", e.to_string());
                    Err("hid api error:")
                }
                Ok(size) => Ok(size)
            }
        }
    
        // read reports using device handle
        fn read(&self, data: &mut Vec<u8>) -> Result<usize, &'static str> {
            const REP_SIZE: usize = PhospheneDev::rep_size();
            let mut arr: [u8; REP_SIZE] = [0; REP_SIZE];
            match self.device.read_timeout(&mut arr[0..REP_SIZE], 
                PhospheneDev::read_timeout_ms()) {
                Err(e) => {
                    println!("{}", e.to_string());
                    Err("hid api error:")
                }
                Ok(size) => Ok(size)
            }
        }
    
        pub fn write_channels(&self, data: &Vec<u8>, offset: u32) {
            if data.len() > (std::u32::MAX as usize) {
                return;
            }
            // channel packet has this layout:
            // [u8: id], [u32: offset], [u32: size], [u8: data.length()]
            // convert u32 numbers to arrays of 4 bytes
            let offset_num_array = u32_to_u8_le(offset);
            let size_num_array = u32_to_u8_le(data.len() as u32);
    
            // first byte: data id, ChannelData = 0x01
            let mut packet_data = vec![1u8];
    
            // push offset bytes to packet data
            for byte in offset_num_array.iter() {
                packet_data.push(*byte);
            }
    
            // push size bytes to packet data
            for byte in size_num_array.iter() {
                packet_data.push(*byte);
            }
    
            // push channel data to vector
            for byte in data.iter() {
                packet_data.push(*byte);
            }

            /*
    
            let frag_write = |data: Vec<u8>| -> Result<usize, &'static str> { self.write(data) };
            pkfrag::fragment(packet_data, PhospheneDev::rep_payload_size() - 1, 
                frag_write).expect("Could not write channel data");
                */
        }
    }
}