extern crate serialport;
extern crate hidapi;

use serialport::prelude::*;
use std::time::Duration;
use std::env;
use std::{thread};
use hidapi::HidApi;

mod phosphene;
use phosphene::device as phdev;

fn listen (port: String) -> Result<(), &'static str> {
    const NUM_BYTES: usize = 297;
    const TIME_OUT_MS: usize = 5000;
    let settings = SerialPortSettings {
        baud_rate: 115200,
        data_bits: DataBits::Eight,
        flow_control: FlowControl::None,
        parity: Parity::None,
        stop_bits: StopBits::One,
        timeout: Duration::from_millis(TIME_OUT_MS as u64),
    };
    let serial_port_res = serialport::open_with_settings(&port, &settings);
    if serial_port_res.is_err() {
        return Err("could not open serial port")
    }
    let mut serial_port = serial_port_res.unwrap();

    match HidApi::new() {
        Ok(api) => {
            match phdev::PhospheneDev::create(api) {
                Some(dev) => {
                    let mut active = true;
                    // clear existing data, in and out
                    serial_port.clear(ClearBuffer::All);
                    let mut time_out = 0;
                    while active {
                        // check how much data arrived
                        match serial_port.bytes_to_read() {
                            Ok(nbytes) => {
                                // if data amount is roughly what our device expects, try to read it
                                if nbytes >= NUM_BYTES as u32 {
                                    let mut data: [u8; NUM_BYTES] = [0xFF; NUM_BYTES];
                                    match serial_port.read_exact(&mut data) {
                                        Ok(_) => {
                                            time_out = 0;
                                            let mut data_v: Vec<u8> = Vec::new();
                                            data_v.extend_from_slice(&data);
                                            // and write to device using channel command
                                            dev.write_channels(&data_v, 0);
                                        },
                                        Err(_e) => {
                                            active = false;
                                        }
                                    }
                                } else {
                                    thread::sleep(Duration::from_millis(1));
                                    time_out += 1;
                                    if time_out >= TIME_OUT_MS {
                                        active = false;
                                    }
                                }
                                
                            },
                            Err(_e) => {
                            
                            }
                        }
                        
                    }
                    
                    Ok(())
                },
                None => {
                    Err("could not open device")
                }
            }
        },
        Err(_e) => {
            Err("could not create hid api")
        }
    }
}

fn main() {

    /*
    let src: Vec<u8> = vec![0,1,2];

    let write = |data: Vec<u8>| -> Result<usize, &'static str> {
        println!("{:?}", data);
        Ok(data.len())
    };

    packetfrag::packetfrag::fragment(src, 1, write);
    */

    let args: Vec<String> = env::args().collect();
    for arg in args {
        // list ports and exit
        if arg == "-l" {
            match serialport::available_ports() {
                Ok(list) => {
                    for port in list {
                        println!("Port: {:?}", port.port_name);
                    }
                },
                Err(e) => {
                    println!("Could not list ports available: {}", e);
                }
            };
            return
        } else {
            // try to open port and listen
            listen(arg);
        }
    }  
}


